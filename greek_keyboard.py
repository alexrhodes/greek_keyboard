"""
Licensed under the MIT License.

Copyright (c) 2023 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import keyboard
from greek_map import GREEK_MAP
import pystray
from PIL import Image

# Needed for terminating sigma after space
END_SIGMA = "ς"
NML_SIGMA = "σ"

"""
Main class that creates the tray icon and
hooks all of the keys.
"""
class GreekKeyboard:
    """
    Constructor
    """
    def __init__(self) -> None:        
        # setup the tray icon
        image = Image.open("tray_icon.png")
        self.icon = pystray.Icon("GK", image, "Greek Keyboard",
					menu=pystray.Menu(
	                    pystray.MenuItem("Exit", self.tray_click)))


        # Set up some variables
        self.last_char = None
        self.greek_map = GREEK_MAP
        self.started = False

        # Load the keys from the map, then
        # hook them
        char_list = []
        # Iterate the map
        for key in GREEK_MAP:
            # Iterate character in the character set
            for s in GREEK_MAP[key]:
                # Create the list of characters
                char_list.append(s)
        # We also need space
        char_list.append(" ")
        
        # Remove duplicates
        self.char_list = list(set(char_list))

        # Setup the hotkeys
        self.setup_hotkeys()

    """
    Tray menu click handler
    """
    def tray_click(self,icon,query):
        if str(query) == "Exit":
            self.exit()
    
    """
    This function sets up hot keys
    """
    def setup_hotkeys(self):
        # Set up the start/stop hotkey
        keyboard.add_hotkey('ctrl+6', lambda: self.start_stop())

    """
    This function terminates the program
    """
    def exit(self):
        self.icon.stop()

    """
    This function handles the enable/disable hotkey
    to start and stop the keyboard
    """
    def start_stop(self):
        if self.started:
            self.stop()
            #print("stopped")
            self.started = False
        else:
            self.start()
            #print("started")
            self.started = True


    """
    This function converts greek to beta code
    """
    def greek_to_code(self,c):
        try:
            s = self.greek_map[c]
            return s
        except:
            return None

    """
    This function converts beta code to greek
    """
    def code_to_greek(self,character_set):
        # Iterate over all of the greek letter keys
        for key in self.greek_map:
            # Grab the set of non-greek characters
            cset = self.greek_map[key]
            # Check if the set matches the calling set
            if set(character_set) == set(cset):
                return key
        return None
    
    """
    This function handles typed characters
    """
    def handle_char(self,c):
        # Check if this is a letter
        if c.isalpha():
            # Check if it can be converted to greek
            greek_char = self.code_to_greek(c)
            if greek_char is not None:
                # This was a valid character, so write it
                keyboard.write(greek_char)
                # Store off the character
                self.last_char = greek_char
                return
            else:
                #print("invalid letter: {}".format(c))
                self.last_char = None
                return
        # Check if this is a control character
        elif c in ["/","\\","=","(",")","|","+"]:
            # Check if the last letter is valid
            if self.last_char is not None:
                # De-compose back into input symbols
                charset = self.greek_to_code(self.last_char)
                
                # Add the new character into the set
                cset = [c]
                newset = charset + cset
                greek_char = self.code_to_greek(newset)
                # If we found a valid greek char,
                # replace the last character in
                # the output with the new character
                if greek_char is not None:
                    keyboard.send('backspace')
                    keyboard.write(greek_char)
                    self.last_char = greek_char
                else:
                    #print("invalid greek char {}".format(c))
                    self.last_char = None
        # Anything else, just append it
        else:
            keyboard.write(c)
            self.last_char = None
            return
    
    """
    This is the keyboard event handler
    """
    def key_handler(self, event):
        if event.event_type == keyboard.KEY_UP:
            # Check the key
            c = event.name

            # Check for the space character
            if c == 'space':
                # If the last character was the 
                # normal sigma, then replace with
                # the terminating sigma
                if self.last_char == NML_SIGMA:
                    keyboard.send('backspace')
                    keyboard.write(END_SIGMA)
                    keyboard.write(" ")
                # Otherwise, just write space
                else:
                    keyboard.write(" ")
            else:
                self.handle_char(c)

    """
    This functions starts the keyboard
    """
    def start(self):
        # Hook the characters
        for c in self.char_list:
            keyboard.hook_key(c,self.key_handler,suppress=True)
    
    """
    This function stops the keyboard
    """
    def stop(self):
        # Unhook characters
        keyboard.unhook_all()

        # Re-add the hotkey
        self.setup_hotkeys()

    """
    This is the main entry loop to the program
    """
    def runloop(self):
        # This never returns
        self.icon.run()

# Create a keybaord
g = GreekKeyboard()

# Run it
g.runloop()
