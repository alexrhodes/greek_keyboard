    Licensed under the MIT License.

    Copyright (c) 2023 - present Alex Rhodes

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

There is a great online tool by [Randy Hoyt](http://randyhoyt.com/contact/) called [Type Greek](https://www.typegreek.com/overview/) that allows the user to type [Beta Code](https://en.wikipedia.org/wiki/Beta_Code), a method for representing ancient Greek text using standard ASCII characters. Typically, Beta Code is typed and later converted. Type Greek, in contrast, allows the user to type Beta Code and see the resulting Greek in real time. Type Greek was the inspiration for this project.

I wanted to create a similar tool for desktop computer use that allowed the user to quickly switch between normal typing and ancient Greek. This facilitates writing Ancient Greek script during the course of any normal desktop use, e.g. word processors, search bars, etc. This post describes how my tool works.

## A Note About Security
Unfortunately, this tool involves mechanisms that are suspciously close to functionality that malware might use. One can examine the code (or just take my word for it) that the software is benign. However, it does involve intercepting and modifying key strokes in real time - sadly very similar to a key logger.  As such, any distributable package (e.g. Pyinstaller executable) will be flagged as malicious and deleted by Windows Defender. I would like to make this readily distributable to non-technical people who'd like to use it, but I do not have the resources to prevent those types of detections. 


That said, it is possible to run the python script directly using Python 3 and the following dependencies (installed with PIP):

    keyboard==0.13.5
    Pillow==9.4.0
    pystray==0.19.4

The code for this project is composed of just two Python files, one of which is just a JSON object that maps Beta Code to Unicode Greek characters. A cursory read of the script reveals that all of the processing is done locally on the user's machine, and no key stroke information is recorded or transmitted in anyway. A detailed explanation of the code is below.

## Using the Tool
Once the application is run, an icon will appear in the Windows tray. By default, the application does not affect normal typing. A hotkey, `ctrl + 6`, will enable or disable the Greek typing mode. When enabled, the user can type Beta Code and it will be converted to Greek script in real time. The following key mappings are used:


| **A** | **B** | **G** | **D** | **E** | **Z** | **H** | **Q** | **I** | **K** | **L** | **M** |
|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
| Α     | Β     | Γ     | Δ     | Ε     | Ζ     | Η     | Θ     | Ι     | Κ     | Λ     | Μ     |
| **N** | **C** | **O** | **P** | **R** | **S** | **T** | **U** | **F** | **X** | **Y** | **W** |
| ν     | Ξ     | Ο     | Π     | ρ     | Σ     | Τ     | Υ     | φ     | Χ     | Ψ     | Ω     |
| **a** | **b** | **g** | **d** | **e** | **z** | **h** | **q** | **i** | **k** | **l** | **m** |
| α     | β     | γ     | δ     | ε     | ζ     | η     | θ     | ι     | κ     | λ     | μ     |
| **n** | **c** | **o** | **p** | **r** | **s** | **t** | **u** | **f** | **x** | **y** | **w** |
| ν     | ξ     | ο     | π     | ρ     | σ (ς) | τ     | υ     | φ     | χ     | ψ     | ω     |

In addition, to add Diacritics, the conventional Beta Code symbols are used:

| **\** | **/** | **=** | **(** | **)** | **\|** | **+** |
|-------|-------|-------|-------|-------|--------|-------|
| ὰ     | ά     | ᾶ     | ἁ     | ἀ     | ᾳ      | ϊ     |

These can be combined in any valid letter combination, e.g.:

| i+\ | a\|/ |
|-----|------|
| ῒ   | ᾴ    |

Additionally, sigma (**σ**) is replaced with with the terminating sigma (**ς**) when it is followed by a space. The keyboard can be toggled between normal and ancient Greek at any time using the `ctrl + 6` hotkey (press `6` while holding the control key). The script can be terminated by right-clicking on the tray icon and selecting `Exit`.

This project will have a detailed writeup on [my website.](https://alexrhodes.io/blog/post/43/)
